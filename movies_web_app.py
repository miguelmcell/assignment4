import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating FLOAT, PRIMARY KEY (id))'
    
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/search_movie', methods=['GET'])
def search_movie():
    msg = request.args.get('search_actor')

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    #cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
    cur.execute("SELECT title, year, actor FROM movie where actor = '" + msg +"'")
    result = cur.fetchall()
    if(len(result) == 0):
        return index('No movies found for actor ' + msg )
    output = ""
    for movies in result:
        title, year, actor = movies
        output += title+', '+str(year)+', '+actor+'<br/>'
    return index(output)

@app.route('/add_movie', methods=['POST'])
def add_movie():
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie where title = '" + title +"'")
    result = cur.fetchall()
    if(len(result) != 0):
        return index('Movie ' + title + ' already exists' )

    try:
        cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) values ('" + year + "', '" + title + "','" + director + "','" + actor + "','" + release_date + "','" + rating + "')")
    except errorcode as error:
        return index('Movie ' + title + ' could not be inserted - ' + str(error))
    cnx.commit()
    return index('Movie ' + title + ' successfully inserted')

@app.route('/update_movie', methods=['POST'])
def update_movie():
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie where title = '" + title +"'")
    result = cur.fetchall()
    if(len(result) == 0):
        return index('Movie ' + title + ' does not exist')
    try:
        cur.execute("UPDATE movie SET year = '" + year + "', title = '" + title + "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = '" + rating + "' WHERE title = '" + title + "'")
    except errorcode as error:
        return index('Movie ' + title + ' could not be inserted - ' + str(error))
    cnx.commit()
    return index('Movie ' + title + ' successfully updated')


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    delete_title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie where title = '" + delete_title +"'")
    result = cur.fetchall()
    if(len(result) == 0):
        return index('Movie with ' + delete_title + ' does not exist' )

    try:
        cur.execute("DELETE FROM movie WHERE title ='" + delete_title +"'")
        cnx.commit()
    except errorcode as error:
        return index("Movie " + delete_title + " could not be deleted - " +  str(error))
    return index("Movie " + delete_title + " successfully deleted")

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM movie WHERE rating = (SELECT MAX(rating) FROM movie)")
    result = cur.fetchall()
    if(len(result) == 0):
        return index('No movies were found')
    output = ""
    for movies in result:
        title, year, actor, director, rating = movies
        output += title + ', ' + str(year) + ', ' + actor + ', ' + director + ', ' + str(rating) + '<br/>'

    return index(output)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM movie WHERE rating = (SELECT MIN(rating) FROM movie)")
    result = cur.fetchall()
    if(len(result) == 0):
        return index('No movies were found')
    output = ""
    for movies in result:
        title, year, actor, director, rating = movies
        output += title + ', ' + str(year) + ', ' + actor + ', ' + director + ', ' + str(rating) + '<br/>'
    
    return index(output)


@app.route("/")
def index(message=''):
    print("Inside index")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    #entries = query_data()
    #print("Entries: %s" % entries)
    return render_template('index.html', message=message)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
